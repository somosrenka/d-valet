package com.somosrenka.developers.d_valet;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.somosrenka.developers.connection.RESTConnection23;
import com.somosrenka.developers.functions.Archivo;
import com.somosrenka.developers.functions.JSONParsers.JsonAutos;
import com.somosrenka.developers.functions.JSONParsers.JsonLogin;
import com.somosrenka.developers.functions.Seguridad;
import com.somosrenka.developers.models.Auto;
import com.somosrenka.developers.models.Usuario;

import org.json.JSONException;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button siguiente;
    EditText email;
    EditText pass1;
    Usuario usuario;
    Seguridad seguridad = new Seguridad();

    @Override
    public void onClick(View view) {
        email = (EditText) findViewById(R.id.login_email_text_view);
        pass1 = (EditText) findViewById(R.id.login_password_text_view);

        TextView textView = (TextView) findViewById(R.id.login_resultado_txt);

        if (validarCampos()) {
            //*
            try {
                if (android.os.Build.VERSION.SDK_INT > 9) {
                    StrictMode.ThreadPolicy policy =
                            new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                }
            } catch (Exception e) {

                //e.printStackTrace();
                Log.i("LoginActivity", e.toString());
                textView.setText(e.toString());
                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
            }
            //*/
            //PREPARANDO LA CONEXION PARA INSERTAR AL USUARIO EN LA BASE DE DATOS

            //Estas lineas osn porque las peticiones aun no son asyncronas,

            //Log.i("LoginActivity", "Agregando el permiso para ejecutar tarea que debe ser asyncrona");
            //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            //StrictMode.setThreadPolicy(policy);

            try {
                Log.i("LoginActivity", "EMPIEZA LA CONEXION");

                Log.i("LoginActivity", email.getText().toString());
                Log.i("LoginActivity", pass1.getText().toString());
                RESTConnection23 connection = new RESTConnection23(RESTConnection23.HOST + "rest-auth/login/");
                //connection.request(RESTConnection23.HOST + "autos/");
                connection.setRequestMethod("POST");
                connection.addRequestHeader("Content-Type", "application/json; charset=UTF-8");
                connection.addRequestParam("password", pass1.getText().toString());
                connection.addRequestParam("username", email.getText().toString());
                connection.addRequestParam("email", email.getText().toString());
                connection.setRequestDoInput(true);
                connection.setRequestDoOutput(true);
                //ESTABLECIENDO LA CONEXION CON EL SERVIDOR
                Log.i("LoginActivity", "Antes de hacer la conexion ");
                connection.LoginConnection();
                Log.i("LoginActivity", String.valueOf(connection.getStatus_()));

                int status = connection.getStatus_();
                Log.i("LoginActivity", "En este punto ya se hizo la peticion y ya se tiene guardada la respuesta");
                Log.i("LoginActivity", Integer.toString(status));
                //Decodificamos la respuesta con la claseJSon correspondiente
                JsonLogin json_login;
                //Log.i("LoginActivity",  "");
                if (connection.status200or201()) {
                    Log.i("LoginActivity", "RESPUESTA 200, EXITO AL LOGUEAR");
                    json_login = new JsonLogin(connection.getResponse());
                    Log.i("LoginActivity", json_login.toString());
                    //CREAMOS EL USUARIO
                    try {
                        seguridad.inizializar(email.getText().toString());
                        byte[] pass = seguridad.encriptarV2(pass1.getText().toString().getBytes());
                        usuario = Usuario.getInstance(email.getText().toString(), json_login.getToken(), pass);
                        String token = json_login.getToken();
                        //usuario = Usuario.getInstance(email.getText().toString(), token);
                        //Toast.makeText(LoginActivity.this, "Mostrando token recibido : " + token, Toast.LENGTH_SHORT).show();
                        Archivo archivo = new Archivo(getApplicationContext(), "userLogin.txt");
                        if (archivo.writeUser(usuario)) {
                            Log.i("SignupActivity", "UUSUARIO GUARDADO EN EL ARCHIVO");
                        } else {
                            Log.i("SignupActivity", "ERROR AL GUARDAR USUARIO EN EL ARCHIVO");
                        }
                        comprobarEstado();
                    } catch (Exception e) {
                        Log.i("SignupActivity", e.getMessage());
                    }


                }
                //  Ocurrio un error durante el logeo
                else {
                    Log.i("LoginActivity", "ERROR AL LOGUEAR");
                    json_login = new JsonLogin(connection.getErrorResponse());
                    Log.i("LoginActivity", json_login.toString());
                    TextView error_text = (TextView) findViewById(R.id.login_resultado_txt);
                    Log.i("LoginActivity", json_login.getErrors());
                    if (error_text != null) {
                        error_text.setText(json_login.getErrors());
                    }
                }
            } catch (IOException e) {
                Log.i("LoginActivity", e.toString());
            } catch (JSONException e) {
                Log.i("LoginActivity", e.toString());
            }
        }
    }

    private void comprobarEstado() {
        try {
            Archivo archivo = new Archivo(getApplicationContext(), "CarList.txt");
            Auto auto = archivo.readAuto();
            if (auto != null) {
                Log.i("ListaAutos", "el archivo esta Correcto");
            } else {
                Log.i("ListaAutos", "el archivo esta vacio");
                ActualizarArchivoCarros();
            }
        } catch (Exception e) {
            ActualizarArchivoCarros();
        }
        Intent redireccion = new Intent(LoginActivity.this, MapsActivity_.class);
        redireccion.putExtra("token", usuario.getToken());
        startActivity(redireccion);
    }

    private void ActualizarArchivoCarros() {
        Log.i("ListaAutos", "Actualizando archivo local de autos");
        try {
            RESTConnection23 connection = null;
            connection = new RESTConnection23(RESTConnection23.HOST + "autos/");
            connection.setRequestMethod("GET");
            //Agregando la autenticacion a la peticion
            connection.addTokenAuthentication(usuario.getToken());
            connection.setJsonRequest(); //indica que la peticion es de tipo json
            connection.setRequestDoInput(true);
            connection.setRequestDoOutput(true);

            Log.i("ListaAutos", "ANTES DE ENVIAR LA PETICION DE AUTOS");
            connection.CarListConnection();
            int status = connection.getStatus_();
            Log.i("ListaAutos", Integer.toString(status));
            //Decodificamos la respuesta con la claseJSon correspondiente
            JsonAutos json_autos;
            if (connection.status200or201()) {
                Log.i("ListaAutos", "Peticion exitosa de lista de carros");
                json_autos = new JsonAutos(connection.getResponse());

                Log.i("ListaAutos", json_autos.toString());
            } else {
                Intent redireccion = new Intent(LoginActivity.this, AddCarActivity.class);
                redireccion.putExtra("token", usuario.getToken());
                startActivity(redireccion);
            }
        } catch (IOException ex) {
            Log.i("ListaAutos", ex.toString());
        } catch (JSONException ex) {
            Log.i("ListaAutos", ex.toString());
        }
        Log.i("ListaAutos", "Terminando de actulizar archivo");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        siguiente = (Button) findViewById(R.id.login_btn_sign_in);
        //Mandamos a llamar el Onclick definido en esta misma clase
        siguiente.setOnClickListener(this);
        //  BUSCAMOS SI EL USUARIO ESTA EN LA BASE DE DATOS, SI ESTA, SE REDIRIGIRA A NOSE DONE

        Log.i("LoginActivity", "SE BUSCARA EL ARCHIVO DE CONFIGURACION DEL USUARIO");
        //Guardando el usuario en un archivo para darle persistencia
        Archivo archivo = new Archivo(getApplicationContext(), "userLogin.txt");
        Usuario usuario = archivo.readUser();
        if (usuario != null) {
            Log.i("LoginActivity", "Existio el usuario");
            Log.i("LoginActivity", usuario.toString());
            //Grabamos el usuario en el objeto Usuario y grabamos la informacion en el archivo
            Usuario.getInstance(usuario.getEmail(), usuario.getToken(), usuario.getPassword());
        } else {
            Log.i("LoginActivity", "No existio el usuario");
        }

        siguiente = (Button) findViewById(R.id.login_btn_sign_in);
        Button carlosMap = (Button) findViewById(R.id.LoginBtnFacebook);
        siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, MapsActivity_.class));
            }
        });


    }


    public boolean validarCampos() {
        TextView error_text = (TextView) findViewById(R.id.login_resultado_txt);
        boolean valido = true;

        try {
            if (TextUtils.isEmpty(email.getText().toString())) {
                error_text.setText(R.string.login_email_error);
                valido = false;
            } else if (TextUtils.isEmpty(pass1.getText().toString())) {
                error_text.setText(R.string.login_contrasena_error);
                valido = false;
            } else {
                error_text.setText("");
            }
            return valido;
        } catch (NullPointerException e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }
        return false;
    }
}