package com.somosrenka.developers.d_valet;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.somosrenka.developers.connection.RESTConnection23;
import com.somosrenka.developers.functions.Archivo;
import com.somosrenka.developers.functions.JSONParsers.JsonRegistro;
import com.somosrenka.developers.functions.Seguridad;
import com.somosrenka.developers.models.Usuario;

/*
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
*/
//import org.json.JSONObject;

public class SignupActivity extends AppCompatActivity
{
    EditText email;
    EditText pass1;
    EditText pass2;
    TextView resultado;
    String servidor;
    Button btnRegistrar;
    Usuario usuario;
    Archivo archivo;
    Seguridad seguridad= new Seguridad();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        try {
            btnRegistrar = (Button) findViewById(R.id.BtnResgistro);
            resultado = (TextView) findViewById(R.id.resultado_signup_text);
            btnRegistrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    email = (EditText) findViewById(R.id.valueEmail);
                    pass1 = (EditText) findViewById(R.id.valuePass1);
                    pass2 = (EditText) findViewById(R.id.valuePass2);
                    servidor = (R.string.SERVIDOR) + "";
                    TextView textViewResultado = (TextView) findViewById(R.id.resultado_signup_text);
                    if (TextUtils.isEmpty(email.getText().toString())) {
                        textViewResultado.setText("Falta el mail");
                    } else if (TextUtils.isEmpty(pass2.getText().toString())) {
                        textViewResultado.setText("Falta el password 2");
                    } else if (TextUtils.isEmpty(pass1.getText().toString())) {
                        textViewResultado.setText("Falta el password 1");
                    } else if (pass1.getText().equals(pass2.getText().toString())) {
                        textViewResultado.setText("El password no empata");
                    } else {
                        try {
                            if (android.os.Build.VERSION.SDK_INT > 9) {
                                StrictMode.ThreadPolicy policy =
                                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                StrictMode.setThreadPolicy(policy);
                            }

                            //PREPARANDO LA CONEXION PARA INSERTAR AL USUARIO EN LA BASE DE DATOS
                            RESTConnection23 connection = new RESTConnection23( RESTConnection23.HOST + "rest-auth/registration/");
                            connection.setRequestMethod("POST");
                            connection.addRequestHeader("Content-Type", "application/json; charset=UTF-8");
                            connection.addRequestParam("username", email.getText().toString());
                            connection.addRequestParam("password1", pass1.getText().toString());
                            connection.addRequestParam("password2", pass1.getText().toString());
                            connection.addRequestParam("email", email.getText().toString());
                            connection.setRequestDoInput(true);
                            connection.setRequestDoOutput(true);

                            //ESTABLECIENDO LA CONEXION CON EL SERVIDOR
                            connection.RegisterConnection();
                            Log.i("RESTConnection23", "En este punto ya se hizo la peticion y ya se tiene guardada la respuesta");
                            Log.i("RESTConnection23", Integer.toString(connection.getStatus_()));

                            //Si La respuesta fue correcta
                            JsonRegistro json;
                            if (connection.status200or201())
                            {
                                json = new JsonRegistro(connection.getResponse());

                                Log.i("SignupActivity", "jObject vale " + json.toString());
                                Log.i("SignupActivity", "Valor de token es" + json.getToken());
                                String token = json.getToken();
                                seguridad.inizializar(email.getText().toString());
                                byte[] pass = seguridad.encriptarV2(pass1.getText().toString().getBytes());
                                usuario = Usuario.getInstance(email.getText().toString(), token, pass);
                                //Log.i("SignupActivity", "Imprimiendo usuario");
                                //Log.i("SignupActivity", usuario.toString());

                                //Guardando el usuario en un archivo para darle persistencia
                                archivo = new Archivo(getApplicationContext(),  "userLogin.txt");
                                if (archivo.writeUser(usuario))
                                {
                                    Log.i("SignupActivity", "UUSUARIO GUARDADO EN EL ARCHIVO");
                                }
                                else
                                {
                                    Log.i("SignupActivity", "ERROR AL GUARDAR USUARIO EN EL ARCHIVO");
                                }
                                //SI EL USUARIO YA TIENE CARROS, SE REDIRECCIONARA A LA VENTANA DEL MAPA
                                // CASO CONTRARIO SE LE REDIRECCIONARA A A INSERTAR CARROS


                                Intent redireccion = new Intent(SignupActivity.this, AddCarActivity.class);
                                redireccion.putExtra("token",token);
                                startActivity(redireccion);
                                Toast.makeText(getApplicationContext(), "Bienvenido " +usuario.getEmail(), Toast.LENGTH_LONG).show();
                                //Log.i("SignupActivity", "La respuesta fue " + stringBuilder);


                            }
                            //Hubo errores en la peticion
                            else
                            {
                                json = new JsonRegistro(connection.getErrorResponse());
                                Log.i("SignupActivity", "Hubo error en el registro ");
                                Log.i("SignupActivity", json.toString());
                                //textViewResultado.setText(json.getErrors().toString());
                                textViewResultado.setText(json.getErrors());
                            }

                        } catch (Exception e)
                        {
                            e.printStackTrace();
                            textViewResultado.setText(e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                            Log.i("SignupActivity", "Error : " + e.toString());
                        }
                        //String url = "http://192.168.0.111:8000/rest-auth/registration/?username=" + email.getText().toString() + "&email=" + email.getText().toString() + "&password1=" + pass1.getText().toString() + "&password2=" + pass2.getText().toString();
                        //new EnviarRegistroNuevo().execute(url);
                        //textView.setText(url);
                    }
                }
            });
        } catch (Exception e) {
            Log.i("SignupActivity", e.getMessage());
            Log.i("SignupActivity", e.getCause().toString());
            Log.i("SignupActivity", e.getClass().toString());
        }
    }
    /*
    private class EnviarRegistroNuevo extends AsyncTask<String, Long, String> {

        protected String doInBackground(String... urls) {
            try {
                return HttpRequest.post(urls[0]).accept("application/json").body();
            } catch (HttpRequest.HttpRequestException exception) {
                return null;
            }
        }

        protected void onPostExecute(String response) {
            TextView textView = (TextView) findViewById(R.id.ResultadoRegistro);
            if (!response.isEmpty()) {
                textView.setText(response.toString());
            }

        }
    }
    */
}
