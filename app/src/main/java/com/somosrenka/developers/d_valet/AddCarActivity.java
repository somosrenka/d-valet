package com.somosrenka.developers.d_valet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.somosrenka.developers.connection.RESTConnection23;
import com.somosrenka.developers.functions.Archivo;
import com.somosrenka.developers.functions.JSONParsers.JsonDatosCarro;
import com.somosrenka.developers.models.Auto;
import com.somosrenka.developers.models.Usuario;

import org.json.JSONException;

import java.io.IOException;

/*
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
*/

public class AddCarActivity extends AppCompatActivity implements View.OnClickListener
{
    Button siguiente;
    Button verListaCarros;
    TextView placa;
    String Splaca;
    TextView color;
    String Scolor;
    TextView modelo;
    String Smodelo;
    TextView resultado;
    String token;

    @Override
    public void onClick(View view)
    {
        placa = (TextView) findViewById(R.id.CarroTxtPlacas);
        Splaca = placa.getText().toString();
        color = (TextView) findViewById(R.id.CarroTxtColor);
        Scolor = color.getText().toString();
        modelo = (TextView) findViewById(R.id.CarroTxtModelo);
        Smodelo = modelo.getText().toString();
        resultado = (TextView) findViewById(R.id.CarroTxtResultado);
        Log.i("AddCarActivity", "Se validaran los campos para guardar carro");
        if (validarCampos())
        {
            try
            {
                Log.i("AddCarActivity", "EMPIEZA LA CONEXCION");
                RESTConnection23 connection = new RESTConnection23( RESTConnection23.HOST + "autos/");
                connection.setRequestMethod("POST");
                connection.addTokenAuthentication(token);
                connection.setJsonRequest();
                connection.addRequestParam("placas", placa.getText().toString());
                connection.addRequestParam("color", color.getText().toString());
                connection.addRequestParam("marca", modelo.getText().toString());
                connection.setRequestDoInput(true);
                connection.setRequestDoOutput(true);
                connection.InsertCarConnection();

                int status = connection.getStatus_();
                Log.i("LoginActivity", "En este punto ya se hizo la peticion y ya se tiene guardada la respuesta");
                Log.i("LoginActivity", Integer.toString(status));
                //Decodificamos la respuesta con la claseJSon correspondiente
                JsonDatosCarro json_carro;
                if (connection.status200or201())
                {
                    json_carro = new JsonDatosCarro(connection.getResponse());
                    //Se creara un objeto Carro y se guardara en un archiivo
                    Auto auto = new Auto(json_carro.getID(), json_carro.getPlacas(),json_carro.getColor(),json_carro.getMarca());
                    Archivo archivo = new Archivo(getApplicationContext(), "CarList.txt");
                    if (archivo.writeCar(auto))
                    {
                        Log.i("SignupActivity", "CARRO GUARDADO EN EL ARCHIVO");
                    }
                    else
                    {
                        Log.i("SignupActivity", "ERROR AL GUARDAR CARRO EN EL ARCHIVO");
                    }
                    //REDIRECCIONAMOS AL VIEW DEL MAPA
                    //REDIRECCIONANDO AL ACTIVITY PARA VER LA LISTA DE AUTOS
                    Intent redireccion = new Intent(AddCarActivity.this, MapsActivity_.class);
                    redireccion.putExtra("token", token);
                    redireccion.putExtra("placas", auto.getPlaca());
                    startActivity(redireccion);
                }
                else
                {
                    //Hubo un error al intentar insertar el auto a la base de datos
                }

            }
            catch (IOException e) {
                Log.i("LoginActivity", e.toString());
            }
            catch (JSONException e) {
                Log.i("LoginActivity", e.toString());
            }

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        try
        {
            Log.i("HomeActitivy","Verificando que el objeto usuario siga como singleton");
            Log.i("HomeActitivy", Usuario.getInstance().toString());
            token = Usuario.getInstance().getToken();
        }
        catch(Exception e )
        {Log.i("HomeActivity",e.toString());}
        setContentView(R.layout.activity_add_car);
        siguiente = (Button) findViewById(R.id.CarroBtnGuardar);
        siguiente.setOnClickListener(this); //asignamos el onclick de esta clase al boton siguiente

        //ELIMINAR ESTO, Y EL BOTON verListaCarros, solo funciono para probar
        // que el servidor retornara los autos del usuario
        verListaCarros = (Button) findViewById(R.id.taco);
        verListaCarros.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                verListaCarros.setEnabled(false);
                //REDIRECCIONANDO AL ACTIVITY PARA VER LA LISTA DE AUTOS
                Log.i("AddCarActivity","Click al boton AddCarActivity");
                Intent redireccion = new Intent(AddCarActivity.this, ListaAutos.class);
                redireccion.putExtra("token", token);
                verListaCarros.setEnabled(true);
                startActivity(redireccion);
            }
        });

    }


    public boolean validarCampos()
    {
        TextView error_text = (TextView) findViewById(R.id.CarroTxtResultado);
        boolean valido=true;
        try {
            if (TextUtils.isEmpty(placa.getText()))
            {
                error_text.setText(R.string.carro_placa_error);
                valido=false;
            }
            else if (TextUtils.isEmpty(color.getText()))
            {
                error_text.setText(R.string.carro_color_error);
                valido=false;
            }
            else if (TextUtils.isEmpty(modelo.getText()))
            {
                error_text.setText(R.string.carro_modelo_error);
                valido=false;
            }
            else if (TextUtils.isEmpty(token))
            {
                error_text.setText(R.string.token_error);
                valido=false;
            }
            else
                {error_text.setText("");}
            return valido;
        }
        catch (NullPointerException e)
            { Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();}
        return false;
    }

}
