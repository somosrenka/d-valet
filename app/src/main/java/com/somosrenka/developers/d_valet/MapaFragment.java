package com.somosrenka.developers.d_valet;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.somosrenka.developers.connection.RESTConnection23;
import com.somosrenka.developers.functions.JSONParsers.JsonAutos;
import com.somosrenka.developers.models.Usuario;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapaFragment extends FragmentActivity implements OnMapReadyCallback {

    private static final long LOCATION_REFRESH_TIME = 1000;
    private static final float LOCATION_REFRESH_DISTANCE = 100;
    private GoogleMap mMap;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    String token;
    String placas = "nada";
    double X = 19.439896;
    double Y = -99.18376;
    String Direccion = "";
    LocationManager locationManager;


    //@TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // flag para status de  GPS
        boolean isGPSEnabled = false;
        // flag para status de RED
        boolean isNetworkEnabled = false;

        setContentView(R.layout.fragment_mapa);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        btn1 = (Button) findViewById(R.id.MapaBtnAccion1);
        btn2 = (Button) findViewById(R.id.MapaBtnAccion2);
        btn3 = (Button) findViewById(R.id.MapaBtnAccion3);
        btn4 = (Button) findViewById(R.id.MapaBtnAccion4);

        //PREGUNTANDO POR LOS PERMISOS
        Log.i("MapaFragment", "Preguntando si hay permisos, si no existen, se pediran");
        //Log.i("MapaFragment","");

        /*
        int hasAccessCoarseLocation = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
        int hasAccessFineLocation = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasAccessCoarseLocation != PackageManager.PERMISSION_GRANTED)
        {
            Log.i("MapaFragment", "No se tiene el permiso de AccessCoarseLocation");
            int REQUEST_CODE_ASK_PERMISSIONS = 123;
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
            return;
        }
        if (hasAccessFineLocation != PackageManager.PERMISSION_GRANTED)
        {
            Log.i("MapaFragment", "No se tiene el permiso de hasAccessFineLocation");
            int REQUEST_CODE_ASK_PERMISSIONS = 123;
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
            return;
        }
        Log.i("MapaFragment", "En este punto ya tenemos acceso a los permisos");
        Log.i("MapaFragment", "Creamos el Manager y el Listen para la ubicacion");


        //CREAMOS EL HANDLER PARA LA LOCALIZACION
        //LocationManager locationManager;
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Localizacion mLocationListener = new Localizacion();
        mLocationListener.setMainActivity(this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME, LOCATION_REFRESH_DISTANCE, mLocationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_REFRESH_TIME, LOCATION_REFRESH_DISTANCE, mLocationListener);

        */
        //Toast.makeText(this, "(Y)", Toast.LENGTH_SHORT).show();
        //Verificando que si halla internet para conectar con el MAPA
        // Obteniendo estado de la red
        /*
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Log.i("MapaFragment", "isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)");
        Log.i("isNetworkEnabled", String.valueOf(isNetworkEnabled));
        */
        /*
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                Localizacion Local = new Localizacion();
                Local.setMainActivity(this);
                mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, Local);
            } else {
                Toast.makeText(this, "Error!!", Toast.LENGTH_SHORT).show();
            }

        */
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMapReady(mMap);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                try
                {
                    try {
                        Intent intent = getIntent();
                        placas = intent.getStringExtra("placas");
                    } catch (Exception ex) {
                        Log.i("Servicio", "Error placas: " + ex.getMessage());
                    }
                    token = Usuario.getInstance().getToken();
                    Log.i("Servicio", "Token: " + token);
                    Log.i("Servicio", "placas: " + placas);
                    Log.i("Servicio", "latitud: " + X);
                    Log.i("Servicio", "longitud: " + Y);
                    if (android.os.Build.VERSION.SDK_INT > 9) {
                        StrictMode.ThreadPolicy policy =
                                new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);
                    }
                    RESTConnection23 connection = null;
                    connection = new RESTConnection23(RESTConnection23.HOST + "servicio/");
                    connection.setRequestMethod("POST");
                    //Agregando la autenticacion a la peticion
                    connection.addTokenAuthentication(token);
                    connection.addRequestParam("placas", placas);
                    connection.addRequestParam("latitud", X + "");
                    connection.addRequestParam("longitud", Y + "");
                    connection.setJsonRequest(); //indica que la peticion es de tipo json
                    connection.setRequestDoInput(true);
                    connection.setRequestDoOutput(true);

                    Log.i("Servicio", "ANTES DE ENVIAR LA PETICION DE Servicio");
                    connection.ServicioConnectio();
                    int status = connection.getStatus_();
                    Log.i("Servicio", Integer.toString(status));
                    //Decodificamos la respuesta con la claseJSon correspondiente
                    JsonAutos json_autos;
                    if (connection.status200or201()) {
                        Log.i("Servicio", "Peticion exitosa de nuevo Servicio");
                    } else {
                        Log.i("Servicio", "Peticion fallida de nuevo Servicio");
                        Log.i("Servicio", connection.getErrorResponse());
                    }

                } catch (IOException e) {
                    Log.i("Servicio", e.toString());
                } catch (JSONException e) {
                    Log.i("Servicio", e.toString());
                }
            }
        });
    }

    //@TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(X, Y);
        MarkerOptions mk = new MarkerOptions().position(sydney).title(Direccion);
        mMap.addMarker(mk);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
        Polygon polygon = mMap.addPolygon(new PolygonOptions()
                .add(
                        new LatLng(19.439896, -99.18376),
                        new LatLng(19.44347, -99.183309),
                        new LatLng(19.444695, -99.191645),
                        new LatLng(19.442585, -99.19197),
                        new LatLng(19.440747, -99.190598)
                )
                .strokeColor(Color.RED)
                .fillColor(Color.BLUE));
        //AGREGANDO MY HUBICACION AL MAPA
        // This gets the button
        int hasAccessCoarseLocation = 0;
        int hasAccessFineLocation = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            hasAccessCoarseLocation = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
            hasAccessFineLocation = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasAccessCoarseLocation != PackageManager.PERMISSION_GRANTED) {
                Log.i("MapaFragment", "No se tiene el permiso de AccessCoarseLocation en OnMapReady");
                int REQUEST_CODE_ASK_PERMISSIONS = 123;
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
                return;
            } else {
                Log.i("MapaFragment", "EXITE permiso de AccessCoarseLocation en OnMapReady");
            }
            if (hasAccessFineLocation != PackageManager.PERMISSION_GRANTED) {
                Log.i("MapaFragment", "No se tiene el permiso de hasAccessFineLocation en OnMapReady");
                int REQUEST_CODE_ASK_PERMISSIONS = 123;
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
                return;
            } else {
                Log.i("MapaFragment", "EXITE permiso de AccessFineLocation en OnMapReady");
            }
        } else {
            checkPermission("", 1, 1);
        }

        //CREAMOS EL HANDLER PARA LA LOCALIZACION
        //LocationManager locationManager;
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Localizacion mLocationListener = new Localizacion();
        mLocationListener.setMainActivity(this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME, LOCATION_REFRESH_DISTANCE, mLocationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_REFRESH_TIME, LOCATION_REFRESH_DISTANCE, mLocationListener);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

    }

    public void setLocation(Location loc) {
        //Obtener la direccion de la calle a partir de la latitud y la longitud
        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        loc.getLatitude(), loc.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);
                    Direccion = DirCalle.getAddressLine(0).toString();
                    //Toast.makeText(this, "Mi direccion es:" + DirCalle.getAddressLine(0), Toast.LENGTH_SHORT).show();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* Aqui empieza la Clase Localizacion */
    public class Localizacion implements LocationListener
    {
        MapaFragment mainActivity;

        public MapaFragment getMainActivity() {
            return mainActivity;
        }

        public void setMainActivity(MapaFragment mainActivity) {
            this.mainActivity = mainActivity;
        }

        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion
            //if ((X != loc.getLatitude()) || (Y != loc.getLongitude())) {
            Log.i("MapaFragment", "OnLocationChanged");
            //Log.i("MapaFragment.OnLocationChanged","OnLocationChanged");
            Log.i("MapaFragment", String.valueOf(loc.getLatitude()));
            Log.i("MapaFragment", String.valueOf(loc.getLongitude()));
            X = loc.getLatitude();
            Y = loc.getLongitude();
            onMapReady(mMap);
            //(String Text = "Mi ubicacion actual es: " + "\n Lat = " + loc.getLatitude() + "\n Long = " + loc.getLongitude();
            this.mainActivity.setLocation(loc);
            //}

        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado
            //Toast.makeText(this, "GPS Desactivo", Toast.LENGTH_SHORT).show();
            Log.i("onProviderDisabled", "HUBICACION DESACTIVADA");
        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado
            //Toast.makeText(null, "GPS Activo", Toast.LENGTH_SHORT).show();
            Log.i("onProviderEnabled", "HUBICACION ACTIVADA");
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // Este metodo se ejecuta cada vez que se detecta un cambio en el
            // status del proveedor de localizacion (GPS)
            // Los diferentes Status son:
            // OUT_OF_SERVICE -> Si el proveedor esta fuera de servicio
            // TEMPORARILY_UNAVAILABLE -> Temporalmente no disponible pero se
            // espera que este disponible en breve
            // AVAILABLE -> Disponible
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getApplicationContext());
            dialogo1.setTitle("Importante");
            dialogo1.setMessage("¿El sistema necesita el gps, Enciendalo por favor?");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    aceptar();
                }
            });
            dialogo1.show();
            Log.i("onStatusChanged", "CAMBIO LA UBICACION DEL USUARIO");

        }

        public void aceptar() {
            Toast t = Toast.makeText(getApplicationContext(), "Gracias", Toast.LENGTH_SHORT);
            t.show();
        }

    }/* Fin de la clase localizacion */
}
