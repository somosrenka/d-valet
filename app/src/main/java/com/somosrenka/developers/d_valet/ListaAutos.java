package com.somosrenka.developers.d_valet;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.somosrenka.developers.connection.RESTConnection23;
import com.somosrenka.developers.functions.JSONParsers.JsonAutos;

import org.json.JSONException;

import java.io.IOException;

public class ListaAutos extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String token = "";
        TextView lista_autos;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_autos);

        //Aqui ya puedo hacer mi desmadre
        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                token = extras.getString("token");
                Toast.makeText(ListaAutos.this, "mostrando token en ListaAutos : " + token, Toast.LENGTH_SHORT).show();
                Log.i("ListaAutos", "mostrando token en datos carro : " + token);
            } else {
                Toast.makeText(ListaAutos.this, "Los extras estan vacios", Toast.LENGTH_SHORT).show();
                Log.i("ListaAutos", "Los extras estan vacios");
            }
        } catch (Exception e) {
            Toast.makeText(ListaAutos.this, "No se encontro el token", Toast.LENGTH_SHORT).show();
            Log.i("ListaAutos", "No se encontro el token");
        }

        ////////////Conectando con el servidor para obtener la lista de carros del usuario
        try
        {
            RESTConnection23 connection = null;
            connection = new RESTConnection23(RESTConnection23.HOST+"autos/");
            connection.setRequestMethod("GET");
            //Agregando la autenticacion a la peticion
            connection.addTokenAuthentication(token);
            connection.setJsonRequest(); //indica que la peticion es de tipo json
            connection.setRequestDoInput(true);
            connection.setRequestDoOutput(true);

            Log.i("ListaAutos", "ANTES DE ENVIAR LA PETICION DE AUTOS");
            connection.CarListConnection();
            int status = connection.getStatus_();
            Log.i("ListaAutos", Integer.toString(status));
            //Decodificamos la respuesta con la claseJSon correspondiente
            JsonAutos json_autos;
            if (connection.status200or201())
            {
                Log.i("ListaAutos", "Peticion exitosa de lista de carros");
                json_autos = new JsonAutos(connection.getResponse());
                Log.i("ListaAutos", json_autos.toString());
                Toast.makeText(ListaAutos.this, json_autos.toString(), Toast.LENGTH_SHORT).show();

                //Escribiendo la lista de autos en el campo de texto del activity
                lista_autos = (TextView) findViewById(R.id.textListaCarros);
                lista_autos.setText("MOSTRANDO AUTOS");
                lista_autos.setText(json_autos.toString());
            }
            else
            {
                Log.i("ListaAutos", "Peticion fallida para lista de carros");
            }

        }
        catch(IOException e) { Log.i("ListaAutos", e.toString()); } catch (JSONException e) { Log.i("ListaAutos", e.toString()); }

    }
}