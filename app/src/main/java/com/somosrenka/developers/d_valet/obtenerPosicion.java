package com.somosrenka.developers.d_valet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.somosrenka.developers.functions.Archivo;
import com.somosrenka.developers.functions.Seguridad;
import com.somosrenka.developers.functions.Utilidades;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class obtenerPosicion extends Activity {

    TextView mensaje1;
    TextView mensaje2;
    Button boton1;
    Button boton2;
    EditText text1;
    Seguridad seguridad;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obtener_posicion);
        mensaje1 = (TextView) findViewById(R.id.gpsLatitud);
        mensaje2 = (TextView) findViewById(R.id.gpsLongitud);
        boton1 = (Button) findViewById(R.id.testBtn1);
        boton2 = (Button) findViewById(R.id.testBtn2);
        text1 = (EditText) findViewById(R.id.testText1);
        seguridad = new Seguridad();
        seguridad.inizializar("isc4.tec@gmail.com");
        boton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    byte[] a1 = seguridad.encriptarV2(text1.getText().toString().getBytes());
                    mensaje1.setText(seguridad.ByteToString(a1));
                } catch (Exception e) {
                    Log.i("Seguridad", e.getMessage());
                }

            }
        });
        boton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    byte[] a1 = seguridad.encriptarV2(text1.getText().toString().getBytes());
                    byte[] a2 = seguridad.desencriptarV2(a1);
                    mensaje2.setText(seguridad.ByteToString(a2));
                } catch (Exception e) {
                    Log.i("Seguridad", e.getMessage());
                }

            }
        });
    }
}