package com.somosrenka.developers.functions.JSONParsers;

import com.google.android.gms.maps.model.LatLng;
import com.somosrenka.developers.models.Area;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class JsonArea {
    private JSONObject jsonAreas;
    private JSONArray jsonCoordenadas;
    private ArrayList<LatLng> cordenadas;

    public JsonArea(String jsonObject) throws JSONException {
        this.jsonAreas = new JSONObject(jsonObject);
        this.createJsonCoordenadas();
    }

    public boolean createJsonCoordenadas() throws JSONException
    {
        if (this.isCoordenadas())
        {
            this.jsonCoordenadas = this.jsonAreas.optJSONArray("ubicacion");
            return true;
        }
        return false;
    }

    public boolean isCoordenadas(){
        return this.jsonAreas.has("ubicacion");
    }

    public JSONObject getJsonAreas() {
        return jsonAreas;
    }

    public void setJsonAreas(JSONObject jsonAreas) {
        this.jsonAreas = jsonAreas;
    }

    public JSONArray getJsonCoordenadas() {
        return jsonCoordenadas;
    }

    public void setJsonCoordenadas(JSONArray jsonCoordenadas) {
        this.jsonCoordenadas = jsonCoordenadas;
    }

    public String toString(){
        return this.jsonAreas.toString() + " : " + this.jsonCoordenadas.toString();
    }

    public ArrayList<LatLng> getCoordenadas() throws JSONException
    {
        ArrayList<LatLng> areas= new ArrayList<>();
        LatLng a;
        int i;
        for (i=0; i < this.jsonCoordenadas.length(); i++)
        {
            JSONObject obj = this.jsonCoordenadas.getJSONObject(i);
            a= new LatLng(obj.getDouble("latitud"), obj.getDouble("longitud"));
            areas.add(a);
        }
        return areas;
    }
}
