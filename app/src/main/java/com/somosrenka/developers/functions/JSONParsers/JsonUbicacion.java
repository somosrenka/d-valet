package com.somosrenka.developers.functions.JSONParsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUbicacion
{
    public JSONObject jObject;

    public JsonUbicacion(String json) throws JSONException
    { jObject = new JSONObject(json); }

    public int getID() throws JSONException
    { return jObject.getInt("id"); }

    public String getLatitud() throws JSONException
    { return jObject.getString("latitud"); }

    public String getLongitud() throws JSONException
    { return jObject.getString("longitud"); }

    public String getPasswordError_test()throws JSONException
    {
        JSONArray arr= jObject.optJSONArray("password1");
        if (arr!=null)
            return arr.getString(0);
        return "";
    }


    public String toString ()
    { return this.jObject.toString(); }

}
