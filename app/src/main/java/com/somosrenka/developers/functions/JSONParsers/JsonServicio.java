package com.somosrenka.developers.functions.JSONParsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by thrashformer on 6/07/16.
 */
public class JsonServicio
{
    public JSONObject jObject;

    public JsonServicio(String json) throws JSONException
        { jObject = new JSONObject(json); }

    public int getID() throws JSONException
        { return jObject.getInt("id"); }

    public double getLatitud() throws JSONException
    { return jObject.getDouble("latitud"); }

    public double getLongitud() throws JSONException
    { return jObject.getDouble("longitud"); }


    private String getGenericError(String key)throws JSONException
    {
        JSONArray arr= jObject.optJSONArray(key);
        if (arr!=null)
            return arr.getString(0);
        return "";
    }

    public String toString ()
        { return this.jObject.toString(); }

}
