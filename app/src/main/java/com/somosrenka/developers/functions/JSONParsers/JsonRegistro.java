package com.somosrenka.developers.functions.JSONParsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class JsonRegistro
{
    public JSONObject jObject;

    public JsonRegistro(String json) throws JSONException
        { jObject = new JSONObject(json); }

    public boolean has_token() throws JSONException
        { return jObject.has("key"); }

    public String getToken() throws JSONException
        { return jObject.getString("key"); }

    public String getEmailError() throws JSONException
        {
            JSONArray arr= jObject.optJSONArray("email");
            if (arr!=null)
                return arr.getString(0);
            return "";
        }

    public String getPasswordError()throws JSONException
    {
        JSONArray arr= jObject.optJSONArray("password1");
        if (arr!=null)
            return arr.getString(0);
        return "";
    }

    public String getPasswordError_test()throws JSONException
    {
        JSONArray arr= jObject.optJSONArray("password1");
        if (arr!=null)
            return arr.getString(0);
        return "";
    }

    public String getErrors() throws JSONException
    {
        return  this.getEmailError()+"\n"+this.getPasswordError();
    }

    public String toString ()
        { return this.jObject.toString(); }
}
