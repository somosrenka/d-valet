package com.somosrenka.developers.functions;

import android.content.Context;
import android.util.Log;

import com.somosrenka.developers.models.AreasServicio;
import com.somosrenka.developers.models.Auto;
import com.somosrenka.developers.models.Usuario;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Archivo
{
    String nombre = "token.txt";
    Context ctx;
    FileOutputStream fos;
    FileInputStream fis;

    public Archivo(Context ctx)
        {this.ctx = ctx;}

    public Archivo(Context ctx, String filename)
    {
        this.nombre = filename;
        this.ctx = ctx;
    }

    public boolean writeUser(Usuario usuario)
        { return writeObject(usuario); }

    public boolean writeCar(Auto auto)
        {return writeObject(auto); }

    public boolean writeAreasServicio(AreasServicio Areas)
    {return writeObject(Areas); }


    public Usuario readUser()
    {
        try
        {
            fis = ctx.openFileInput(nombre);
            ObjectInputStream obs = new ObjectInputStream(fis);
            Usuario usuario = (Usuario) obs.readObject();
            return usuario;
        }
        catch (FileNotFoundException e)
            {Log.i("Archivo",e.toString());}
        catch (IOException e)
            {Log.i("Archivo",e.toString());}
        catch (ClassNotFoundException e)
            {Log.i("Archivo",e.toString());}
        return null;
    }
    public Auto readAuto()
    {
        try
        {
            fis = ctx.openFileInput(nombre);
            ObjectInputStream obs = new ObjectInputStream(fis);
            Auto auto = (Auto) obs.readObject();
            return auto;
        }
        catch (FileNotFoundException e)
            {Log.i("Archivo",e.toString());}
        catch (IOException e)
            {Log.i("Archivo",e.toString());}
        catch (ClassNotFoundException e)
            { Log.i("Archivo",e.toString());}
        return null;
    }

    private boolean writeObject(Object object)
    {
        try
        {
            fos = ctx.openFileOutput(nombre, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
            return true;
        }
        catch (FileNotFoundException e)
            {Log.e("FileNotFound", e.getMessage());}
        catch (IOException e)
            { Log.e("IOException", e.getMessage());}
        return false;
    }

    public AreasServicio readAreas(){
        try{
            fis = ctx.openFileInput(nombre);
            ObjectInputStream obs = new ObjectInputStream(fis);
            AreasServicio areasServicio = (AreasServicio) obs.readObject();
            return areasServicio;
        }catch (FileNotFoundException e) {
            Log.i("Archivo", e.toString());
        } catch (IOException e) {
            Log.i("Archivo", e.toString());
        } catch (ClassNotFoundException e) {
            Log.i("Archivo", e.toString());
        }
        return null;
    }
}



/*
//  NOTAS PARA ESCRIBIR Y GUARDAR OBJETOS
Saving (w/o exception handling code):

FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
ObjectOutputStream os = new ObjectOutputStream(fos);
os.writeObject(this);
os.close();
fos.close();

Loading (w/o exception handling code):

FileInputStream fis = context.openFileInput(fileName);
ObjectInputStream is = new ObjectInputStream(fis);
SimpleClass simpleClass = (SimpleClass) is.readObject();
is.close();
fis.close();


                                    out = new ObjectOutputStream(new FileOutputStream(new File(getFilesDir(),"")+File.separator+filename));
                                    out.writeObject(myPersonObject);
                                    out.close();

 */
