package com.somosrenka.developers.functions.JSONParsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JsonLogin
{
    public JSONObject jObject;

    public JsonLogin(String json) throws JSONException
    { jObject = new JSONObject(json); }

    public boolean has_token() throws JSONException
    { return jObject.has("key"); }

    public String getToken() throws JSONException
    { return jObject.getString("key"); }

    public String getError() throws JSONException
    {
        JSONArray arr= jObject.optJSONArray("non_field_errors");
        if (arr!=null)
            return arr.getString(0);
        return "";
    }

    public String getErrors() throws JSONException
        { return  this.getError(); }

    public String toString()
        { return jObject.names().toString(); }
}
