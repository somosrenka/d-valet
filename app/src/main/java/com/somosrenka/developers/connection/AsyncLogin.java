package com.somosrenka.developers.connection;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

public class AsyncLogin extends AsyncTask<RESTConnection23, Void, Void>
{


    @Override
    protected void onPreExecute()
            { super.onPreExecute(); }


    @Override
    protected Void doInBackground(RESTConnection23... connection)
    {
        CookieHandler.setDefault(new CookieManager( null, CookiePolicy.ACCEPT_ALL));
        try {
            Log.i("RESTConnection23", "Conternido que se enviara en la peticion ");
            Log.i("RESTConnection23", connection[0].getConnection().getRequestMethod());
            Log.i("RESTConnection23", connection[0].getConnection().toString());
            Log.i("RESTConnection23", connection[0].getJson_().toString());
            OutputStream os = connection[0].getConnection().getOutputStream();
            os.write(connection[0].getJson_().toString().getBytes("UTF-8"));
            Log.i("RESTConnection23", "Ya se escribieron los parametros del json a la peticion");
            Log.i("RESTConnection23", Integer.toString(connection[0].getConnection().getResponseCode()));
            os.close();
            //connection[0].LoginConnection();
            Log.i("AsyncLogin", "Conecto correctmente");
        } catch (IOException e)
        {
            Log.i("AsyncLogin", "Error al conectar en AsyncLogin");
            Log.i("AsyncLogin", e.toString());
            e.printStackTrace();
        }
        //return connection[0];
        /*
        try {
            HttpGet httppost = new HttpGet("YOU URLS TO JSON");
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(httppost);

            // StatusLine stat = response.getStatusLine();
            int status = response.getStatusLine().getStatusCode();

            if (status == 200)
            {
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                JSONObject jsono = new JSONObject(data);
                return true;
            }

        } catch (IOException e)
            { e.printStackTrace(); }
        catch (JSONException e)
            { e.printStackTrace(); }
        return false;
        */
        return null;
    }

    protected void onPostExecute(RESTConnection23 connection) throws IOException
    {
        Log.i("AsyncLogin", "onPostExecute");
        Log.i("AsyncLogin", connection.toString());
        Log.i("AsyncLogin", Integer.toString(connection.getStatus_()));
    }
}



