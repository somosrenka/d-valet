package com.somosrenka.developers.Places;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;

public class ValetPlace
{
    private Place place;
    public ValetPlace (Place place)
        {
            this.setPlace(place);}

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public LatLng getLatLng ()
        {return this.place.getLatLng();}

}
