package com.somosrenka.developers.models;
import java.io.Serializable;

/**
 * Created by thrashformer on 6/07/16.
 */
public class Ubicacion implements Serializable
{
    private int id;
    private float latitud;
    private float longitud;

    public Ubicacion()
    {}

    public Ubicacion(int id, float latitud, float longitud)
    {
        this.id = id;
        this.latitud = latitud;
        this.longitud = longitud;
    }



    public int getId()
        {return id;}

    public void setId(int id)
        {this.id = id;}

    public float getLatitud()
        {return latitud;}

    public void setLatitud(float latitud)
        {this.latitud = latitud;}

    public float getLongitud()
        {return longitud;}

    public void setLongitud(float longitud)
        {this.longitud = longitud;}
}
