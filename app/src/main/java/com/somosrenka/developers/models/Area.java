package com.somosrenka.developers.models;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;

public class Area implements Serializable{
    private String descripcion;
    private ArrayList<LatLng> cordenadas;

    public Area() {
        cordenadas = new ArrayList<>();
        descripcion="";
    }

    public Area(String descripcion, ArrayList<LatLng> cordenadas) {
        this.descripcion = descripcion;
        this.cordenadas=cordenadas;
    }


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<LatLng> getCordenadas() {
        return cordenadas;
    }

    public void setCordenadas(ArrayList<LatLng> cordenadas) {
        this.cordenadas = cordenadas;
    }

    public void addCordenadas(LatLng cordenadas) {
        this.cordenadas.add(cordenadas);
    }
}

