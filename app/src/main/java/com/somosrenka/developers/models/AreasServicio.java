package com.somosrenka.developers.models;

import java.io.Serializable;
import java.util.ArrayList;

public class AreasServicio implements Serializable {
    ArrayList<Area> areas;

    public void AreasSevicio()
        {areas = new ArrayList<>();}

    public void AreasSevicio(ArrayList<Area> Areas) {
        this.areas = Areas;
    }

    public ArrayList<Area> getAreas() {
        return areas;
    }

    public void setAreas(ArrayList<Area> Areas) {
        this.areas = Areas;
    }

    public void addArea(Area newArea) {
        areas.add(newArea);
    }

}